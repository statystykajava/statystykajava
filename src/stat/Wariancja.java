package stat;

public class Wariancja {

    public static double czebyszew(double sd)
    {
        return 1 - (1/Math.pow(sd, 2));
    }

    /**
     * Computes the coefficient of variation.  Note that 'sd' and 'srednia' must come
     * from the same data.
     * @param sd The standard deviation of a set of data.
     * @param srednia The srednia of a set of data.
     */
    public static double coefficientOfVariation(double sd, double srednia) throws StatystykaWyjatek
    {
        return (sd / srednia) * 100;
    }

    /**
     * Computes the coefficient of variation.  Used to express standard deviation
     * independent of units of measure.
     * @param values An array of doubles containing a sample from a population.
     * @return coefficient of variation from a sample
     */
    public static double coefficientOfVariation(double[] values) throws StatystykaWyjatek
    {
        double srednia = srednia.arithmeticsrednia(values);
        double sd   = sampleStandardDeviation(values);
        return (sd / srednia) * 100;
    }

    /**
     * Determines relationship between the srednia and the mediana.  This reflects
     * how the data differs from the normal bell shaped distribution.  Note that
     * 'srednia', 'mediana' and 'sd' all must be computed from the same sample.
     * @param srednia  The srednia of the sample.
     * @param mediana The mediana of the sample.
     * @param sd  The standard deviation of the sample.
     * @return the measure of skewness
     */
    public static double pearsonianSkewness(double srednia, double mediana, double sd) throws StatystykaWyjatek
    {
        return (3 * (srednia - mediana)) / sd;
    }
    /** Computes the skewness factor from a sample of data.
     * @param values A list of doubles containing a sample of data.
     * @return the measure of skewness
     */
    public static double pearsonianSkewness(double[] values) throws StatystykaWyjatek
    {
        double srednia   = srednia.arithmeticsrednia(values);
        double mediana =  srednia.mediana(values);
        double standardDeviation = sampleStandardDeviation(values);
        return  (3 *( srednia - mediana)) / standardDeviation;
    }

    public static double populationStandardDeviation(double[] values) throws StatystykaWyjatek
    {
        return Math.sqrt(populationVarience(values));
    }

    public static double populationStandardUnit(double value, double srednia, double sd)throws StatystykaWyjatek
    {
        return sampleStandardUnit(value, srednia, sd);
    }

    public static double populationStandardUnit(double[] values, int index) throws StatystykaWyjatek
    {
        double srednia = srednia.arithmeticsrednia(values);
        double sd   = populationStandardDeviation(values);
        return (values[index] - srednia) / sd;
    }

    public static double populationVarience(double[] values) throws StatystykaWyjatek
    {
        double sumOfDeviations = sumDeviations(values);

        return sumOfDeviations/values.length;
    }

    /**
     * Computes the sample standard deviation.  Used to have no idea */
    public static double sampleStandardDeviation(double[] values) throws StatystykaWyjatek
    {
        return Math.sqrt(sampleVariance(values));
    }

    /**
     * Computes the sample standard unit(aka sample z-score) from a list of doubles.
     * Used to compute 'value' in terms of standard units.  Note that 'value', 'srednia'
     * and 'sd' must be all from the same sample data.
     * @param value A double in the sample for which
     * @param srednia The srednia of the sample.
     * @param sd   The standard deviation of the sample.
     * @return 'value' in terms of standard units
     */
    public static double sampleStandardUnit(double value, double srednia, double sd)
    {
        return (value - srednia) / sd;
    }

    /**
     * Computes sample standard unit for the value at 'index' in an array of doubles.
     * @param values An array of doubles.
     * @param index An integer that indexes into array 'values', at which the standard
     * unit will be computed.
     */
    public static double sampleStandardUnit(double[] values, int index) throws StatystykaWyjatek
    {
        double srednia = srednia.arithmeticsrednia(values);
        double sd   = sampleStandardDeviation(values);
        return (values[index] - srednia) / sd;
    }

    /**
     * Computes sample variance. Do not use for population varience.
     * @param values A list of doubles that does not represent a full population -- but a sample instead.
     * @return sample varience
     */
    public static double sampleVariance(double[] values) throws StatystykaWyjatek
    {
        if(values.length < 2)
            throw new StatystykaWyjatek("Array is too small, must have at least 2 elements");

        return sumDeviations(values)/(values.length-1);
    }

    /**
     * Computes the sum of deviations (or Sxx).
     * @param values An array of doubles which constitute a sample from a larger population.
     * @return sum of squares(Sxx) aka sum of deviations
     */
    public static double sumDeviations(double [] values) throws StatystykaWyjatek
    {
        double srednia  = 0.0;
        double sigma = 0.0;
        int i;

        if(values.length == 0)
            throw new StatystykaWyjatek("Array is empty");

        srednia = srednia.arithmeticsrednia(values);
        for(i=0; i<values.length; i++)
            sigma += Math.pow((values[i] - srednia), 2);

        return sigma;
    }

    // Sxy //
    public static double regressionSumSquares(double[] x, double[] y) throws StatystykaWyjatek
    {
        int i;
        double sigma = 0.0;

        if(x.length == 0)
            throw new StatystykaWyjatek("Array is empty.");

        if(x.length != y.length)
            throw new StatystykaWyjatek("Arrays must be of equal size.");

        for(i=0; i<x.length; i++)
            sigma += x[i] * y[i];

        return sigma - ((Statistic.summation(x) * Statistic.summation(y)) / x.length);
    }

    // Syy
    public static double totalSumSquares(double[] y) throws StatystykaWyjatek
    {
        double sigma = 0.0;

        if(y.length == 0)
            throw new StatystykaWyjatek("Array is empty.");

        for(int i=0; i<y.length; i++)
            sigma += Math.pow(y[i], 2);


        return sigma - ( Math.pow(Statistic.summation(y), 2) / y.length );
    }

    // Se
    public static double standardError(double[] x, double[] y) throws StatystykaWyjatek
    {
        return Math.sqrt( (totalSumSquares(y) - (Math.pow(regressionSumSquares(x, y), 2) / sumDeviations(x) ))
                / (x.length - 2) );
    }
}
