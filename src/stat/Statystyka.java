package stat;

import java.util.Locale;


public class Statystyka {
    /** Przy liczbach większych od zdefinowanego progu do obliczeń będzie wykorzystana Aproksymacja Stirlinga
     * @param n liczba do obliczenia
     * @return silia z n
     * @throws StatystykaWyjatek for n < 0
     * */

    public final static int PROG = 50;

    public static double silnia(double n) throws StatystykaWyjatek {
        double result = 1;

        if (n < 0) {
            throw new StatystykaWyjatek("Nie mozna obliczyć silni dla wartości ujemnych!");
        } else if (n > PROG ) {
            result = AproksymacjaStirlinga(n);
        } else {
            for (int i = 2; i <= n; i++) {
                result *= i;
            }
        }
        return result;
    }

    /**
     * Dla dużych wartości n przyjmujemy: n! ~= sqrt(2*pi*n)*(n/e)^n
     * @param n - liczba do obliczenia
     * @return Wynik aproksymacji Stirlinga dla n!
     */
    public static double AproksymacjaStirlinga(double n) {
        return Math.pow(n, n) * Math.exp(-n) * Math.sqrt(2 * Math.PI * n);
    }

    /**
     * Permutacja: nPr = n!/(n-r)!
     * @param n liczba elementów
     * @param r liczba wybranych elementów
     * @return liczba permutacji P(n,r)
     * @throws StatystykaWyjatek if n < r or r < 0 or n < 0
     */


    public static double permutacja(double n, double r) throws StatystykaWyjatek {
        double perm;
        if (n < 0 || r < 0 || n < r) {
            throw new StatystykaWyjatek("Niepoprawne parametry (" + n + "," + r + "): Funkcja permutacji wymaga n >= r & n >= 0 & r >= 0");
        } else if (n == r) {
            perm = silnia(n);  // dla n == 0 wynik za pomocą funkcji silnia
        } else {
            perm = (silnia(n) / silnia(n - r));
        }

        return perm;
    }

    /**
     * Kombinacja: nCr = n!/(n-r)!r!
     * @param n liczba elementów
     * @param r length of combination sequence
     * @return liczba kombinacji C(n,r)
     * @throws StatystykaWyjatek if n < r or n < 0 or r < 0
     */
    public static double combination(double n, double r) throws StatystykaWyjatek {
        double comb = 0.0;
        if (n < 0 || r < 0 || n < r) {
            throw new StatystykaWyjatek("Illegal parameters (" + n + "," + r + "): Combination function requires n >= r and n >= 0 and r >= 0");
        } else if (n == r) {
            // shortcut -- also covers the n == 0 case since r by above rule must also == 0
            comb = 1;
        } else {
            comb =(silnia(n, r) * (1.0/silnia(r)));
        }
        return comb;
    }

    /**
     * Suma elementów tablicy
     * @param values wartosci w tablicy double
     * @return suma wszystkich elementów tablicy
     * @throws StatystykaWyjatek dla pustej tablicy
     */
    public static double sumowanie(double values[]) throws StatystykaWyjatek {
        int i;
        double sigma = 0.0;

        if (values.length < 1) {
            throw new StatystykaWyjatek("Pusta tablicza");
        }

        for (i = 0; i < values.length; i++) {
            sigma += values[i];
        }

        return sigma;
    }

    /**
     * Suma elementów tablicy
     * @param values wartosci w tablicy long
     * @return suma wszystkich elementów tablicy
     * @throws StatystykaWyjatek dla pustej tablicy
     */
    public static double sumowanie(long values[]) throws StatystykaWyjatek {
        int i;
        double sigma = 0.0;

        if (values.length < 1) {
            throw new StatystykaWyjatek("Pusta tablicza");
        }

        for (i = 0; i < values.length; i++) {
            sigma += values[i];
        }

        return sigma;
    }

}
}

