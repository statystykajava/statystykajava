package stat;

public class StatystykaWyjatek{

    /** Domyslny konstruktor */
    public StatystykaWyjatek() {
        super();
    }

    /**
     * Konstruktor ze Stringiem opisującym wyjątek
     * @param errorString String z opisem
     */
    public StatystykaWyjatek(String errorString) {
        super(errorString);
    }

}
