package stat;

public class Korelacja {
    /**
     * Oblicza współczynnik korelacji dla próbki lub populacyjnych danych.
     * @param x
     * @param y
     * @return współczynnik x oraz y
     * @throws StatystykaWyjatek
     */
    public static double wspolczynnik(double[] x, double[] y) throws StatystykaWyjatek
    {
        return Wariancja.regressionSumSquares(x, y) /
                Math.sqrt( Wariancja.sumDeviations(x) * Wariancja.totalSumSquares(y));
    }

    public static double rozkladFishera(double[] x, double[] y) throws StatystykaWyjatek
    {
        return rozkladFishera(wspolczynnik(x, y));
    }

    public static double rozkladFishera(double wspolczynnik) throws StatystykaWyjatek
    {
       Prawdopodobienstwo.validatePrawdopodobienstwo(wspolczynnik);
        return 0.5 * (Math.log( (1 + wspolczynnik) / (1 - wspolczynnik) ));
    }

    public static double normalCorrelation(double[] x, double[] y, double p) throws StatystykaWyjatek
    {
        double wspolczynnik = wspolczynnik(x, y);
        double fisher     = rozkladFishera(wspolczynnik);
        double uz          = rozkladFishera(p);

        return (fisher - uz) * Math.sqrt(x.length - 3);
    }

    public static double wielokrotnyWspolczynnikKorelacji()
    { return 1.00; }
}
